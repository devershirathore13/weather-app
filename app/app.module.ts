import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import{HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './nav/nav.component';
import{WeatherSearchComponent,WeatherListComponent,WeatherItemComponent} from './weather/index';
import{SidebarComponent} from './sidebar/sidebar.components';
import{WeatherService} from './weather/weather.service';
import  './shared/rxjs-extensions';
import {ProfileService} from './sidebar/profile.service';
@NgModule({
  imports: [ BrowserModule,
                   FormsModule,
                   AppRoutingModule,
                   HttpModule,
                   ReactiveFormsModule
                   ],
  declarations: [ AppComponent,
                          HomeComponent,
                          NavbarComponent,
                          WeatherSearchComponent,
                          WeatherListComponent,
                          WeatherItemComponent,
                          SidebarComponent
                          ],
  providers: [ WeatherService ,
    ProfileService],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }