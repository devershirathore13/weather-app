import { Injectable } from '@angular/core';
import {Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { IWeather } from './weather';
import { WEATHER_ITEMS } from "./weather.data";

@Injectable()
export class WeatherService {
    private URL: string = 'http://api.openweathermap.org/data/2.5/weather?q=';
    private KEY: string = 'e3af5d13f26362ed5c05b4aadae03de7';
    private IMP: string = '&units=imperial';

    constructor(private _http: Http) { }

    getWeatherItems() {
      return WEATHER_ITEMS;
    }

    addWeatherItem(weatherItem: IWeather) {
      WEATHER_ITEMS.push(weatherItem)
      console.log(WEATHER_ITEMS);
    }

    searchWeatherData(cityName: string): Observable<IWeather[]> {
      return this
                ._http
                .get(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
                // .get(this.URL + cityName + '&APPID=' + this.KEY + this.IMP)
                .map((res: Response) => <IWeather[]> res.json())
                .do(res => console.log('Weather Data Object ' + JSON.stringify(res)))
                .catch(error => {
                  console.error(error);
                  return Observable.throw(error.json())
                });
    }
}